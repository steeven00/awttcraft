
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;

import net.minecraft.world.level.levelgen.feature.Feature;

import net.mcreator.awttcraft.world.features.ores.ShelloreFeature;
import net.mcreator.awttcraft.world.features.ores.EnderiumOreFeature;
import net.mcreator.awttcraft.AwttmodMod;

@Mod.EventBusSubscriber
public class AwttmodModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, AwttmodMod.MODID);
	public static final RegistryObject<Feature<?>> SHELLORE = REGISTRY.register("shellore", ShelloreFeature::feature);
	public static final RegistryObject<Feature<?>> ENDERIUM_ORE = REGISTRY.register("enderium_ore", EnderiumOreFeature::feature);
}
