
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModSounds {
	public static final DeferredRegister<SoundEvent> REGISTRY = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, AwttmodMod.MODID);
	public static final RegistryObject<SoundEvent> AESTSFIGHT = REGISTRY.register("aestsfight",
			() -> new SoundEvent(new ResourceLocation("awttmod", "aestsfight")));
	public static final RegistryObject<SoundEvent> TESSERACT_WORKING_SOUND = REGISTRY.register("tesseract_working_sound",
			() -> new SoundEvent(new ResourceLocation("awttmod", "tesseract_working_sound")));
	public static final RegistryObject<SoundEvent> ACDC_TNT = REGISTRY.register("acdc-tnt",
			() -> new SoundEvent(new ResourceLocation("awttmod", "acdc-tnt")));
	public static final RegistryObject<SoundEvent> AWAYCANTEXPLAIN = REGISTRY.register("awaycantexplain",
			() -> new SoundEvent(new ResourceLocation("awttmod", "awaycantexplain")));
	public static final RegistryObject<SoundEvent> UPTOPWNFUNK = REGISTRY.register("uptopwnfunk",
			() -> new SoundEvent(new ResourceLocation("awttmod", "uptopwnfunk")));
	public static final RegistryObject<SoundEvent> HARDERBETTERFASTERSTRONGER = REGISTRY.register("harderbetterfasterstronger",
			() -> new SoundEvent(new ResourceLocation("awttmod", "harderbetterfasterstronger")));
}
