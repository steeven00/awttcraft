
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.Block;

import net.mcreator.awttcraft.block.entity.TesseractBlockBlockEntity;
import net.mcreator.awttcraft.block.entity.DraconicAnvilItemBlockEntity;
import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModBlockEntities {
	public static final DeferredRegister<BlockEntityType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, AwttmodMod.MODID);
	public static final RegistryObject<BlockEntityType<?>> TESSERACT_BLOCK = register("tesseract_block", AwttmodModBlocks.TESSERACT_BLOCK,
			TesseractBlockBlockEntity::new);
	public static final RegistryObject<BlockEntityType<?>> DRACONIC_ANVIL_ITEM = register("draconic_anvil_item", AwttmodModBlocks.DRACONIC_ANVIL_ITEM,
			DraconicAnvilItemBlockEntity::new);

	private static RegistryObject<BlockEntityType<?>> register(String registryname, RegistryObject<Block> block,
			BlockEntityType.BlockEntitySupplier<?> supplier) {
		return REGISTRY.register(registryname, () -> BlockEntityType.Builder.of(supplier, block.get()).build(null));
	}
}
