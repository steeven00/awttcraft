
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.awttcraft.item.SoftShellItemItem;
import net.mcreator.awttcraft.item.ShellItemItem;
import net.mcreator.awttcraft.item.ShellFragmentsItem;
import net.mcreator.awttcraft.item.RockdiscitemItem;
import net.mcreator.awttcraft.item.PopDiscitemItem;
import net.mcreator.awttcraft.item.OtterworldItem;
import net.mcreator.awttcraft.item.OtterCoreItem;
import net.mcreator.awttcraft.item.JazzDiscitemItem;
import net.mcreator.awttcraft.item.EnderiumIngotItem;
import net.mcreator.awttcraft.item.ElectroDiskItemItem;
import net.mcreator.awttcraft.item.AwttArmorItem;
import net.mcreator.awttcraft.item.AwakenedOtterCoreItem;
import net.mcreator.awttcraft.item.AestsFightItem;
import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, AwttmodMod.MODID);
	public static final RegistryObject<Item> SHELLORE = block(AwttmodModBlocks.SHELLORE, AwttmodModTabs.TAB_AWTTER_CRAFT_ORE);
	public static final RegistryObject<Item> SHELL_FRAGMENTS = REGISTRY.register("shell_fragments", () -> new ShellFragmentsItem());
	public static final RegistryObject<Item> SHELL_ITEM = REGISTRY.register("shell_item", () -> new ShellItemItem());
	public static final RegistryObject<Item> SOFT_SHELL_ITEM = REGISTRY.register("soft_shell_item", () -> new SoftShellItemItem());
	public static final RegistryObject<Item> OTTER_CORE = REGISTRY.register("otter_core", () -> new OtterCoreItem());
	public static final RegistryObject<Item> TESSERACT_BLOCK = block(AwttmodModBlocks.TESSERACT_BLOCK, AwttmodModTabs.TAB_AWTTER_CRAFT_BLOCKS);
	public static final RegistryObject<Item> AESTS_FIGHT = REGISTRY.register("aests_fight", () -> new AestsFightItem());
	public static final RegistryObject<Item> ENDERIUM_ORE = block(AwttmodModBlocks.ENDERIUM_ORE, AwttmodModTabs.TAB_AWTTER_CRAFT_ORE);
	public static final RegistryObject<Item> ENDERIUM_INGOT = REGISTRY.register("enderium_ingot", () -> new EnderiumIngotItem());
	public static final RegistryObject<Item> AWAKENED_OTTER_CORE = REGISTRY.register("awakened_otter_core", () -> new AwakenedOtterCoreItem());
	public static final RegistryObject<Item> DRACONIC_ANVIL_ITEM = block(AwttmodModBlocks.DRACONIC_ANVIL_ITEM,
			AwttmodModTabs.TAB_AWTTER_CRAFT_BLOCKS);
	public static final RegistryObject<Item> ROCKDISCITEM = REGISTRY.register("rockdiscitem", () -> new RockdiscitemItem());
	public static final RegistryObject<Item> POP_DISCITEM = REGISTRY.register("pop_discitem", () -> new PopDiscitemItem());
	public static final RegistryObject<Item> JAZZ_DISCITEM = REGISTRY.register("jazz_discitem", () -> new JazzDiscitemItem());
	public static final RegistryObject<Item> ELECTRO_DISK_ITEM = REGISTRY.register("electro_disk_item", () -> new ElectroDiskItemItem());
	public static final RegistryObject<Item> AWTT_ARMOR_HELMET = REGISTRY.register("awtt_armor_helmet", () -> new AwttArmorItem.Helmet());
	public static final RegistryObject<Item> AWTT_ARMOR_CHESTPLATE = REGISTRY.register("awtt_armor_chestplate", () -> new AwttArmorItem.Chestplate());
	public static final RegistryObject<Item> AWTT_ARMOR_LEGGINGS = REGISTRY.register("awtt_armor_leggings", () -> new AwttArmorItem.Leggings());
	public static final RegistryObject<Item> AWTT_ARMOR_BOOTS = REGISTRY.register("awtt_armor_boots", () -> new AwttArmorItem.Boots());
	public static final RegistryObject<Item> OTTERWORLD = REGISTRY.register("otterworld", () -> new OtterworldItem());
	public static final RegistryObject<Item> OTTER_PORTAL_FRAME_BLOCK = block(AwttmodModBlocks.OTTER_PORTAL_FRAME_BLOCK,
			AwttmodModTabs.TAB_AWTTER_CRAFT_BLOCKS);

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
