
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.extensions.IForgeMenuType;

import net.minecraft.world.inventory.MenuType;

import net.mcreator.awttcraft.world.inventory.TesseractGuiMenu;
import net.mcreator.awttcraft.world.inventory.GuiDraconicAnvilMenu;
import net.mcreator.awttcraft.world.inventory.GamemodeChangerGuiMenu;
import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModMenus {
	public static final DeferredRegister<MenuType<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.MENU_TYPES, AwttmodMod.MODID);
	public static final RegistryObject<MenuType<TesseractGuiMenu>> TESSERACT_GUI = REGISTRY.register("tesseract_gui",
			() -> IForgeMenuType.create(TesseractGuiMenu::new));
	public static final RegistryObject<MenuType<GuiDraconicAnvilMenu>> GUI_DRACONIC_ANVIL = REGISTRY.register("gui_draconic_anvil",
			() -> IForgeMenuType.create(GuiDraconicAnvilMenu::new));
	public static final RegistryObject<MenuType<GamemodeChangerGuiMenu>> GAMEMODE_CHANGER_GUI = REGISTRY.register("gamemode_changer_gui",
			() -> IForgeMenuType.create(GamemodeChangerGuiMenu::new));
}
