
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class AwttmodModTabs {
	public static CreativeModeTab TAB_AWTTER_CRAFT_ORE;
	public static CreativeModeTab TAB_AWTTER_CRAFT_MATERIAL;
	public static CreativeModeTab TAB_AWTTER_CRAFT_MUSIC;
	public static CreativeModeTab TAB_AWTTER_CRAFT_BLOCKS;
	public static CreativeModeTab TAB_AWTT_CRAFT_ARMOR;
	public static CreativeModeTab TAB_OTTER_TOOLS;

	public static void load() {
		TAB_AWTTER_CRAFT_ORE = new CreativeModeTab("tabawtter_craft_ore") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModBlocks.ENDERIUM_ORE.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_AWTTER_CRAFT_MATERIAL = new CreativeModeTab("tabawtter_craft_material") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModItems.SOFT_SHELL_ITEM.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_AWTTER_CRAFT_MUSIC = new CreativeModeTab("tabawtter_craft_music") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModItems.AESTS_FIGHT.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_AWTTER_CRAFT_BLOCKS = new CreativeModeTab("tabawtter_craft_blocks") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModBlocks.TESSERACT_BLOCK.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_AWTT_CRAFT_ARMOR = new CreativeModeTab("tabawtt_craft_armor") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModItems.AWTT_ARMOR_HELMET.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
		TAB_OTTER_TOOLS = new CreativeModeTab("tabotter_tools") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(AwttmodModItems.OTTERWORLD.get());
			}

			@Override
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
