
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.client.gui.screens.MenuScreens;

import net.mcreator.awttcraft.client.gui.TesseractGuiScreen;
import net.mcreator.awttcraft.client.gui.GuiDraconicAnvilScreen;
import net.mcreator.awttcraft.client.gui.GamemodeChangerGuiScreen;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class AwttmodModScreens {
	@SubscribeEvent
	public static void clientLoad(FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			MenuScreens.register(AwttmodModMenus.TESSERACT_GUI.get(), TesseractGuiScreen::new);
			MenuScreens.register(AwttmodModMenus.GUI_DRACONIC_ANVIL.get(), GuiDraconicAnvilScreen::new);
			MenuScreens.register(AwttmodModMenus.GAMEMODE_CHANGER_GUI.get(), GamemodeChangerGuiScreen::new);
		});
	}
}
