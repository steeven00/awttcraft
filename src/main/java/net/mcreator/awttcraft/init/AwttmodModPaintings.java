
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.entity.decoration.PaintingVariant;

import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModPaintings {
	public static final DeferredRegister<PaintingVariant> REGISTRY = DeferredRegister.create(ForgeRegistries.PAINTING_VARIANTS, AwttmodMod.MODID);
	public static final RegistryObject<PaintingVariant> BORED_SHADE_NFT = REGISTRY.register("bored_shade_nft", () -> new PaintingVariant(48, 48));
}
