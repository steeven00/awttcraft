
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.awttcraft.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.mcreator.awttcraft.block.TesseractBlockBlock;
import net.mcreator.awttcraft.block.ShelloreBlock;
import net.mcreator.awttcraft.block.OtterworldPortalBlock;
import net.mcreator.awttcraft.block.OtterPortalFrameBlockBlock;
import net.mcreator.awttcraft.block.EnderiumOreBlock;
import net.mcreator.awttcraft.block.DraconicAnvilItemBlock;
import net.mcreator.awttcraft.AwttmodMod;

public class AwttmodModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, AwttmodMod.MODID);
	public static final RegistryObject<Block> SHELLORE = REGISTRY.register("shellore", () -> new ShelloreBlock());
	public static final RegistryObject<Block> TESSERACT_BLOCK = REGISTRY.register("tesseract_block", () -> new TesseractBlockBlock());
	public static final RegistryObject<Block> ENDERIUM_ORE = REGISTRY.register("enderium_ore", () -> new EnderiumOreBlock());
	public static final RegistryObject<Block> DRACONIC_ANVIL_ITEM = REGISTRY.register("draconic_anvil_item", () -> new DraconicAnvilItemBlock());
	public static final RegistryObject<Block> OTTERWORLD_PORTAL = REGISTRY.register("otterworld_portal", () -> new OtterworldPortalBlock());
	public static final RegistryObject<Block> OTTER_PORTAL_FRAME_BLOCK = REGISTRY.register("otter_portal_frame_block",
			() -> new OtterPortalFrameBlockBlock());
}
