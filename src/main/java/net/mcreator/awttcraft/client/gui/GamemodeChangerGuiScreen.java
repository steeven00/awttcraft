
package net.mcreator.awttcraft.client.gui;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.Component;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.Minecraft;

import net.mcreator.awttcraft.world.inventory.GamemodeChangerGuiMenu;
import net.mcreator.awttcraft.network.GamemodeChangerGuiButtonMessage;
import net.mcreator.awttcraft.AwttmodMod;

import java.util.HashMap;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.systems.RenderSystem;

public class GamemodeChangerGuiScreen extends AbstractContainerScreen<GamemodeChangerGuiMenu> {
	private final static HashMap<String, Object> guistate = GamemodeChangerGuiMenu.guistate;
	private final Level world;
	private final int x, y, z;
	private final Player entity;

	public GamemodeChangerGuiScreen(GamemodeChangerGuiMenu container, Inventory inventory, Component text) {
		super(container, inventory, text);
		this.world = container.world;
		this.x = container.x;
		this.y = container.y;
		this.z = container.z;
		this.entity = container.entity;
		this.imageWidth = 176;
		this.imageHeight = 105;
	}

	private static final ResourceLocation texture = new ResourceLocation("awttmod:textures/screens/gamemode_changer_gui.png");

	@Override
	public void render(PoseStack ms, int mouseX, int mouseY, float partialTicks) {
		this.renderBackground(ms);
		super.render(ms, mouseX, mouseY, partialTicks);
		this.renderTooltip(ms, mouseX, mouseY);
	}

	@Override
	protected void renderBg(PoseStack ms, float partialTicks, int gx, int gy) {
		RenderSystem.setShaderColor(1, 1, 1, 1);
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShaderTexture(0, texture);
		this.blit(ms, this.leftPos, this.topPos, 0, 0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);

		RenderSystem.setShaderTexture(0, new ResourceLocation("awttmod:textures/screens/server-icon.png"));
		this.blit(ms, this.leftPos + 69, this.topPos + 60, 0, 0, 32, 32, 32, 32);

		RenderSystem.disableBlend();
	}

	@Override
	public boolean keyPressed(int key, int b, int c) {
		if (key == 256) {
			this.minecraft.player.closeContainer();
			return true;
		}
		return super.keyPressed(key, b, c);
	}

	@Override
	public void containerTick() {
		super.containerTick();
	}

	@Override
	protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
	}

	@Override
	public void onClose() {
		super.onClose();
		Minecraft.getInstance().keyboardHandler.setSendRepeatsToGui(false);
	}

	@Override
	public void init() {
		super.init();
		this.minecraft.keyboardHandler.setSendRepeatsToGui(true);
		this.addRenderableWidget(new Button(this.leftPos + 5, this.topPos + 5, 67, 20, Component.literal("Creative"), e -> {
			if (true) {
				AwttmodMod.PACKET_HANDLER.sendToServer(new GamemodeChangerGuiButtonMessage(0, x, y, z));
				GamemodeChangerGuiButtonMessage.handleButtonAction(entity, 0, x, y, z);
			}
		}));
		this.addRenderableWidget(new Button(this.leftPos + 99, this.topPos + 5, 67, 20, Component.literal("Survival"), e -> {
			if (true) {
				AwttmodMod.PACKET_HANDLER.sendToServer(new GamemodeChangerGuiButtonMessage(1, x, y, z));
				GamemodeChangerGuiButtonMessage.handleButtonAction(entity, 1, x, y, z);
			}
		}));
		this.addRenderableWidget(new Button(this.leftPos + 3, this.topPos + 27, 72, 20, Component.literal("Spectate"), e -> {
			if (true) {
				AwttmodMod.PACKET_HANDLER.sendToServer(new GamemodeChangerGuiButtonMessage(2, x, y, z));
				GamemodeChangerGuiButtonMessage.handleButtonAction(entity, 2, x, y, z);
			}
		}));
		this.addRenderableWidget(new Button(this.leftPos + 96, this.topPos + 27, 72, 20, Component.literal("adventure"), e -> {
			if (true) {
				AwttmodMod.PACKET_HANDLER.sendToServer(new GamemodeChangerGuiButtonMessage(3, x, y, z));
				GamemodeChangerGuiButtonMessage.handleButtonAction(entity, 3, x, y, z);
			}
		}));
	}
}
