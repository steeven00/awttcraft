package net.mcreator.awttcraft.procedures;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.effect.MobEffectInstance;

import net.mcreator.awttcraft.init.AwttmodModItems;

public class AwttArmorHelmetTickEventProcedure {
	public static void execute(Entity entity) {
		if (entity == null)
			return;
		double previousRecipe = 0;
		if ((entity instanceof LivingEntity _entGetArmor ? _entGetArmor.getItemBySlot(EquipmentSlot.HEAD) : ItemStack.EMPTY)
				.getItem() == AwttmodModItems.AWTT_ARMOR_HELMET.get()) {
			if ((entity instanceof LivingEntity _entGetArmor ? _entGetArmor.getItemBySlot(EquipmentSlot.CHEST) : ItemStack.EMPTY)
					.getItem() == AwttmodModItems.AWTT_ARMOR_CHESTPLATE.get()) {
				if ((entity instanceof LivingEntity _entGetArmor ? _entGetArmor.getItemBySlot(EquipmentSlot.LEGS) : ItemStack.EMPTY)
						.getItem() == AwttmodModItems.AWTT_ARMOR_LEGGINGS.get()) {
					if ((entity instanceof LivingEntity _entGetArmor ? _entGetArmor.getItemBySlot(EquipmentSlot.FEET) : ItemStack.EMPTY)
							.getItem() == AwttmodModItems.AWTT_ARMOR_BOOTS.get()) {
						if (entity instanceof LivingEntity _livEnt ? _livEnt.isBlocking() : false) {
							if (entity instanceof LivingEntity _entity)
								_entity.addEffect(
										new MobEffectInstance(MobEffects.REGENERATION, (int) Double.POSITIVE_INFINITY, 1, (false), (false)));
						} else {
							if (entity instanceof LivingEntity _entity)
								_entity.removeEffect(MobEffects.REGENERATION);
						}
					}
				}
			}
		}
		if ((entity instanceof LivingEntity _entGetArmor ? _entGetArmor.getItemBySlot(EquipmentSlot.HEAD) : ItemStack.EMPTY)
				.getItem() == AwttmodModItems.AWTT_ARMOR_HELMET.get()) {
			if (entity instanceof LivingEntity _entity)
				_entity.addEffect(new MobEffectInstance(MobEffects.CONDUIT_POWER, 1, 0));
		} else {
			if (entity instanceof LivingEntity _entity)
				_entity.removeEffect(MobEffects.CONDUIT_POWER);
		}
	}
}
