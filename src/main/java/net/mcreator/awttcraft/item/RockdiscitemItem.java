
package net.mcreator.awttcraft.item;

import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.world.item.RecordItem;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.awttcraft.init.AwttmodModTabs;

public class RockdiscitemItem extends RecordItem {
	public RockdiscitemItem() {
		super(0, () -> ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("awttmod:acdc-tnt")),
				new Item.Properties().tab(AwttmodModTabs.TAB_AWTTER_CRAFT_MUSIC).stacksTo(1).rarity(Rarity.RARE), 0);
	}
}
