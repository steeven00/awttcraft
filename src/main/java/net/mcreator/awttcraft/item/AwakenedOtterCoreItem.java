
package net.mcreator.awttcraft.item;

import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.mcreator.awttcraft.init.AwttmodModTabs;

public class AwakenedOtterCoreItem extends Item {
	public AwakenedOtterCoreItem() {
		super(new Item.Properties().tab(AwttmodModTabs.TAB_AWTTER_CRAFT_MATERIAL).stacksTo(64).rarity(Rarity.EPIC));
	}

	@Override
	public UseAnim getUseAnimation(ItemStack itemstack) {
		return UseAnim.EAT;
	}
}
