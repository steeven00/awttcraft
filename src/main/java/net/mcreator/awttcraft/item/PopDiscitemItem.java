
package net.mcreator.awttcraft.item;

import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.world.item.RecordItem;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.awttcraft.init.AwttmodModTabs;

public class PopDiscitemItem extends RecordItem {
	public PopDiscitemItem() {
		super(0, () -> ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("awttmod:uptopwnfunk")),
				new Item.Properties().tab(AwttmodModTabs.TAB_AWTTER_CRAFT_MUSIC).stacksTo(1).rarity(Rarity.RARE), 0);
	}
}
