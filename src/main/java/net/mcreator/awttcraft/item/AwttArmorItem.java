
package net.mcreator.awttcraft.item;

import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.world.level.Level;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Entity;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.awttcraft.procedures.AwttArmorLeggingsTickEventProcedure;
import net.mcreator.awttcraft.procedures.AwttArmorHelmetTickEventProcedure;
import net.mcreator.awttcraft.procedures.AwttArmorBootsTickEventProcedure;
import net.mcreator.awttcraft.procedures.AwttArmorBodyTickEventProcedure;
import net.mcreator.awttcraft.init.AwttmodModTabs;
import net.mcreator.awttcraft.init.AwttmodModItems;

public abstract class AwttArmorItem extends ArmorItem {
	public AwttArmorItem(EquipmentSlot slot, Item.Properties properties) {
		super(new ArmorMaterial() {
			@Override
			public int getDurabilityForSlot(EquipmentSlot slot) {
				return new int[]{13, 15, 16, 11}[slot.getIndex()] * 25;
			}

			@Override
			public int getDefenseForSlot(EquipmentSlot slot) {
				return new int[]{2, 5, 6, 2}[slot.getIndex()];
			}

			@Override
			public int getEnchantmentValue() {
				return 9;
			}

			@Override
			public SoundEvent getEquipSound() {
				return ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("item.armor.equip_netherite"));
			}

			@Override
			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(AwttmodModItems.AWAKENED_OTTER_CORE.get()));
			}

			@Override
			public String getName() {
				return "awtt_armor";
			}

			@Override
			public float getToughness() {
				return 3f;
			}

			@Override
			public float getKnockbackResistance() {
				return 0f;
			}
		}, slot, properties);
	}

	public static class Helmet extends AwttArmorItem {
		public Helmet() {
			super(EquipmentSlot.HEAD, new Item.Properties().tab(AwttmodModTabs.TAB_AWTT_CRAFT_ARMOR));
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "awttmod:textures/models/armor/awttarmor6__layer_1.png";
		}

		@Override
		public void onArmorTick(ItemStack itemstack, Level world, Player entity) {
			AwttArmorHelmetTickEventProcedure.execute(entity);
		}
	}

	public static class Chestplate extends AwttArmorItem {
		public Chestplate() {
			super(EquipmentSlot.CHEST, new Item.Properties().tab(AwttmodModTabs.TAB_AWTT_CRAFT_ARMOR));
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "awttmod:textures/models/armor/awttarmor6__layer_1.png";
		}

		@Override
		public void onArmorTick(ItemStack itemstack, Level world, Player entity) {
			AwttArmorBodyTickEventProcedure.execute(entity);
		}
	}

	public static class Leggings extends AwttArmorItem {
		public Leggings() {
			super(EquipmentSlot.LEGS, new Item.Properties().tab(AwttmodModTabs.TAB_AWTT_CRAFT_ARMOR));
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "awttmod:textures/models/armor/awttarmor6__layer_2.png";
		}

		@Override
		public void onArmorTick(ItemStack itemstack, Level world, Player entity) {
			AwttArmorLeggingsTickEventProcedure.execute(entity);
		}
	}

	public static class Boots extends AwttArmorItem {
		public Boots() {
			super(EquipmentSlot.FEET, new Item.Properties().tab(AwttmodModTabs.TAB_AWTT_CRAFT_ARMOR));
		}

		@Override
		public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
			return "awttmod:textures/models/armor/awttarmor6__layer_1.png";
		}

		@Override
		public void onArmorTick(ItemStack itemstack, Level world, Player entity) {
			AwttArmorBootsTickEventProcedure.execute(entity);
		}
	}
}
